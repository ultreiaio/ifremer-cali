package io.ultreia.ifremer.cali;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.jaxx.runtime.application.ApplicationBoot;
import org.nuiton.jaxx.runtime.application.ApplicationConfiguration;
import org.nuiton.jaxx.runtime.application.test.BootArguments;
import org.nuiton.jaxx.runtime.application.test.BootTestHelper;

import java.io.File;
import java.util.Optional;

/**
 * Created by tchemit on 13/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CaliTestRule implements TestRule {

    private static final Log log = LogFactory.getLog(CaliTestRule.class);
    private static final String BOARD_NAME = "BigFin DFS/2-A17B";
    private static final String BOARD_URL = "btspp://00066662A17B:1;authenticate=true;encrypt=true;master=true";

    private ApplicationBoot<CaliConfig, CaliContext> boot;

    private File testDirectory;

    @Override
    public Statement apply(Statement base, Description description) {
        before(description);
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    public BigFinDevice getDevice() {
        CaliContext context = getContext();
        BigFinDevice device = null;
        try {
            device = context.getBigFinDevices().openDeviceFromUrl(BOARD_NAME, BOARD_URL);
            log.info("Found board: " + device);
        } catch (Exception e) {
            log.error("can't get device", e);
        }
        Assume.assumeTrue("Can't find board: " + BOARD_NAME, device != null);
        return device;
    }

    public ApplicationBoot<CaliConfig, CaliContext> getBoot() {
        return boot;
    }


    public CaliConfig getConfiguration() {
        return boot.getConfiguration();
    }

    public CaliContext getContext() {
        return boot.getContext();
    }

    private void before(Description description) {
        ApplicationBoot.BOOT_LOG_PREFIX = "Application.boot [Cali]";
        testDirectory = BootTestHelper.getTestBasedir(description.getTestClass());

        String[] bootArguments = Optional.ofNullable(BootTestHelper.getBootArgumentsAnnotation(description.getTestClass())).map(BootArguments::value).orElse(new String[0]);
        ImmutableList<String> args = ImmutableList.<String>builder()
                .add("--option")
                .add(ApplicationConfiguration.DefaultOptions.DATA_DIRECTORY.getKey())
                .add(testDirectory.getAbsolutePath())
                .add(bootArguments)
                .build();
        boot = ApplicationBoot.create(new CaliTestBoot(args.toArray(new String[args.size()])));
        boot.start();
    }

    private void after(Description description) {
        boot.shutdown();
    }

}
