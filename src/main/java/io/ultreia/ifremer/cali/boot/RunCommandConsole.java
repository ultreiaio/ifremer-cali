package io.ultreia.ifremer.cali.boot;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliConfig;
import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.api.io.BigFinCommand;
import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import org.nuiton.jaxx.runtime.application.command.CommandSupport;

import java.util.Scanner;

/**
 * Created by tchemit on 13/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class RunCommandConsole extends CommandSupport<CaliConfig, CaliContext> {

    private String boardName;

    public void execute(String boardName) throws InterruptedException {
        this.boardName = boardName;
        start();
    }

    @Override
    public void run() {

        BigFinDevice device = getContext().getBigFinDevices().openDevice(boardName);
        device.addBigFinDeviceMessageListener(event -> System.out.printf("Message received: %s%n", event.getMessage()));
        System.out.printf("Starts command on %s%n", device.getName());
        System.out.println("Enter 'q', to exit");
        Scanner scanner = new Scanner(System.in);

        String line;

        while (!(line = scanner.nextLine().trim()).equals("q")) {
            if (line.isEmpty()) {
                continue;
            }
            if (line.startsWith("!")) {
                line = line.substring(1).trim() + '\r';
            }
            System.out.printf("Send command: %s %n", line);
            try {
                device.sendCommand(new BigFinCommand(line));
            } catch (Exception e) {
                System.err.println("Command " + line + " failed: " + e.getMessage());
                e.printStackTrace();
            }
        }
        System.out.println("Exit...");
        getContext().getBoot().shutdown();
    }

}
