package io.ultreia.ifremer.cali.api.io.record;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.event.EventListenerList;
import java.io.Closeable;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * To read some records from a ichtyometer in feed mode.
 * <p>
 * Created on 1/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class BigFinDeviceRecordConsumer implements Runnable, Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BigFinDeviceRecordConsumer.class);
    /**
     * To keep list of {@link BigFinDeviceRecordListener} listeners.
     */
    private final EventListenerList listenerList;
    /**
     * To generate some feed record from the rax record send by the board.
     */
    private final BigFinDeviceRecordFactory recordFactory;
    /**
     * BigFin device.
     */
    private BigFinDevice device;
    /**
     * Flag to stop the runnable.
     */
    private boolean stop;
    /**
     * Log prefix.
     */
    private final String prefix;
    /**
     * Internal thread to wait for next message.
     * <p>
     * <b>Note:</b> we can't use here simple runner in thread pool executor, since the thread must be stop here asap.
     */
    private Thread thread;

    public BigFinDeviceRecordConsumer(BigFinDevice device) {
        this.device = device;
        this.prefix = String.format("Measure consumer [device: %s]", device.getName());
        this.listenerList = new EventListenerList();
        this.recordFactory = new BigFinDeviceRecordFactory();
    }

    public void start(ThreadPoolExecutor threadPoolExecutor) {
        Preconditions.checkState(device.isOpen(), "device must be opened");
        thread = Objects.requireNonNull(threadPoolExecutor).getThreadFactory().newThread(this);
        thread.start();
        log.info(String.format("%s Ready to read measures.", prefix));
    }

    @Override
    public void close() {
        this.stop = true;
        removeAllFeedModeReaderListeners();
        try {
            thread.join(500);
        } catch (InterruptedException e) {
            log.error(String.format("%s Could not close thread", prefix), e);
        } finally {
            thread = null;
        }
    }

    public void addFeedModeReaderListener(BigFinDeviceRecordListener listener) {
        listenerList.add(BigFinDeviceRecordListener.class, listener);
    }

    public void removeFeedModeReaderListener(BigFinDeviceRecordListener listener) {
        listenerList.remove(BigFinDeviceRecordListener.class, listener);
    }

    public void removeAllFeedModeReaderListeners() {
        for (BigFinDeviceRecordListener listener : listenerList.getListeners(BigFinDeviceRecordListener.class)) {
            listenerList.remove(BigFinDeviceRecordListener.class, listener);
        }
    }

    @Override
    public void run() {
        log.info(String.format("%s start %s", prefix, this));
        while (!stop) {
            try {
                BigFinDeviceRecordSupport readerRecord = readRecord();
                if (!stop && readerRecord != null && readerRecord instanceof BigFinDeviceMeasureRecord) {
                    log.info(String.format("%s new measure record received %s", prefix, readerRecord));
                    BigFinDeviceRecordEvent e = null;
                    for (BigFinDeviceRecordListener listener : listenerList.getListeners(BigFinDeviceRecordListener.class)) {
                        if (e == null) {
                            e = new BigFinDeviceRecordEvent(this, (BigFinDeviceMeasureRecord) readerRecord);
                        }
                        listener.recordRead(e);
                    }
                }
            } catch (InterruptedException e) {
                log.info("Closing...");
            } catch (Exception e) {
                log.error("Could not read record", e);
            }
        }
        log.info(String.format("%s stop %s", prefix, this));
    }

    private BigFinDeviceRecordSupport readRecord() throws InterruptedException {
        if (stop) {
            return null;
        }
        log.debug(String.format("%s Waiting to read new measure", prefix));
        String message = device.readMessage();
        BigFinDeviceRecordSupport readerRecord = null;
        if (!stop) {
            readerRecord = recordFactory.newRecord(message, new Date());
        }
        log.debug(String.format("%s New measure: %s", prefix, readerRecord));
        return readerRecord;
    }
}
