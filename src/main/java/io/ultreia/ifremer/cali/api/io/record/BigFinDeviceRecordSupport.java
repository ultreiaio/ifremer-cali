package io.ultreia.ifremer.cali.api.io.record;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Created on 12/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public abstract class BigFinDeviceRecordSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Incoming raw record.
     */
    protected final String record;
    private final Date date;

    protected BigFinDeviceRecordSupport(String record, Date date) {
        this.record = record;
        this.date = date;
        Preconditions.checkState(isValid());
    }

    public abstract boolean isValid();

    public String getRecord() {
        return record;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("record", record)
                .append("date", date)
                .toString();
    }
}
