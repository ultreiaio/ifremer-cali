package io.ultreia.ifremer.cali.api;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class Calibration extends AbstractJavaBean {
    /**
     * Incoming board.
     */
    private final Board board;
    /**
     * Is calibration initialized?
     */
    private boolean initialize;
    /**
     * Step one value.
     */
    private Integer stepOneRequest;
    /**
     * Step one result value (raw value 1).
     */
    private Integer stepOneResult;
    /**
     * Step two value.
     */
    private Integer stepTwoRequest;
    /**
     * Step two result value (raw value 2).
     */
    private Integer stepTwoResult;

    protected Calibration(Board board) {
        this.board = board;
    }

    public Board getBoard() {
        return board;
    }

    public String getBoardName() {
        return board.getBoardName();
    }

    public Integer getStepOneRequest() {
        return stepOneRequest;
    }

    public void setStepOneRequest(Integer stepOneRequest) {
        this.stepOneRequest = stepOneRequest;
        firePropertyChange("stepOneRequest", stepOneRequest);
        firePropertyChange("stepOneValid", isStepOneValid());
        firePropertyChange("valid", isValid());
    }

    public Integer getStepOneResult() {
        return stepOneResult;
    }

    public void setStepOneResult(Integer stepOneResult) {
        this.stepOneResult = stepOneResult;
        firePropertyChange("stepOneResult", stepOneResult);
        firePropertyChange("stepOneValid", isStepOneValid());
        firePropertyChange("valid", isValid());
    }

    public Integer getStepTwoRequest() {
        return stepTwoRequest;
    }

    public void setStepTwoRequest(Integer stepTwoRequest) {
        this.stepTwoRequest = stepTwoRequest;
        firePropertyChange("stepTwoRequest", stepTwoRequest);
        firePropertyChange("stepTwoValid", isStepTwoValid());
        firePropertyChange("valid", isValid());
    }

    public Integer getStepTwoResult() {
        return stepTwoResult;
    }

    public void setStepTwoResult(Integer stepTwoResult) {
        this.stepTwoResult = stepTwoResult;
        firePropertyChange("stepTwoResult", stepTwoResult);
        firePropertyChange("stepTwoValid", isStepTwoValid());
        firePropertyChange("valid", isValid());
    }

    public boolean isInitialize() {
        return initialize;
    }

    public void setInitialize(boolean initialize) {
        this.initialize = initialize;
        firePropertyChange("initialize", initialize);
    }

    public boolean isValid() {
        return isStepOneValid() && isStepTwoValid();
    }

    public boolean isStepOneValid() {
        return getStepOneResult() != null;
    }

    public boolean isStepTwoValid() {
        return getStepTwoResult() != null;
    }

}
