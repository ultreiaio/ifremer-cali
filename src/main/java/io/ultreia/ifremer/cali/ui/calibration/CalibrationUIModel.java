package io.ultreia.ifremer.cali.ui.calibration;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.api.Board;
import io.ultreia.ifremer.cali.api.Calibration;
import io.ultreia.ifremer.cali.ui.board.BoardUIModel;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.nuiton.jaxx.runtime.application.ApplicationBoot;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class CalibrationUIModel extends Calibration {

    private Thread thread;
    private String initializeResultText;
    private String stepOneResultText;
    private String stepTwoResultText;

    public CalibrationUIModel(BoardUIModel boardUIModel) {
        super(boardUIModel);
    }

    @Override
    public BoardUIModel getBoard() {
        return (BoardUIModel) super.getBoard();
    }

    @Override
    public void setInitialize(boolean initialize) {
        super.setInitialize(initialize);
        setInitializeResultText(initialize ? t("cali.calibration.initializeResult") : null);
    }

    @Override
    public void setStepOneResult(Integer stepOneResult) {
        super.setStepOneResult(stepOneResult);
        setStepOneResultText(stepOneResult == null ? null : t("cali.calibration.stepOneResult", stepOneResult));
    }

    @Override
    public void setStepTwoResult(Integer stepTwoResult) {
        super.setStepTwoResult(stepTwoResult);
        setStepTwoResultText(stepTwoResult == null ? null : t("cali.calibration.stepTwoResult", stepTwoResult));
    }

    public String getStepTwoResultText() {
        return stepTwoResultText;
    }

    public void setStepTwoResultText(String stepTwoResultText) {
        this.stepTwoResultText = stepTwoResultText;
        firePropertyChange("stepTwoResultText", stepTwoResultText);
    }

    public String getStepOneResultText() {
        return stepOneResultText;
    }

    public void setStepOneResultText(String stepOneResultText) {
        this.stepOneResultText = stepOneResultText;
        firePropertyChange("stepOneResultText", stepOneResultText);
    }

    public String getInitializeResultText() {
        return initializeResultText;
    }

    public void setInitializeResultText(String initializeResultText) {
        this.initializeResultText = initializeResultText;
        firePropertyChange("initializeResultText", initializeResultText);
    }

    public void start() {
        (thread = ApplicationBoot.boot().getThreadFactory().newThread(this::startThread, "calibration", false)).start();
    }

    public void stop() {
        CaliContext.context().getBigFinCommandEngine().stopLastCommand();
        if (thread != null && thread.isAlive()) {
            try {
                thread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startThread() {
        CaliContext.context().getBoardApi().processCalibration(this);
    }
}
