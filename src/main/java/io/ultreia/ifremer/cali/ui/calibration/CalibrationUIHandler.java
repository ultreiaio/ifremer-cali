package io.ultreia.ifremer.cali.ui.calibration;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.RunCali;
import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import io.ultreia.ifremer.cali.ui.JLabelEditor;
import io.ultreia.ifremer.cali.ui.board.BoardUI;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerManager;

import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CalibrationUIHandler implements UIHandler<CalibrationUI> {

    private final CalibrationUI ui;

    CalibrationUIHandler(CalibrationUI ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(CalibrationUI ui) {
        ComponentInitializerManager.get().apply(this.ui);
        CalibrationUIModel model = ui.getModel();

        model.addPropertyChangeListener("initialize", e -> onInitializeChanged((boolean) e.getNewValue()));
        model.addPropertyChangeListener("stepOneValid", e -> onStepOneValidChanged((boolean) e.getNewValue()));
        model.addPropertyChangeListener("stepTwoValid", e -> onStepTwoValidChanged((boolean) e.getNewValue()));
        model.addPropertyChangeListener("valid", e -> onValidChanged((boolean) e.getNewValue()));

        onInitializeChanged(false);
        onStepOneValidChanged(false);
        onStepTwoValidChanged(false);
        onValidChanged(false);
    }

    public void start(BoardUI parent) {
        ((CaliActionSupport) ui.getCancelOrQuit().getAction()).installAction(parent);
        ((CaliActionSupport) ui.getStartMeasureMode().getAction()).installAction(parent);
        CalibrationUIModel model = ui.getModel();
        Integer stepOneRequest = model.getStepOneRequest();
        ui.getStepOneRequestLabel().setText(t("cali.calibration.ask.value", stepOneRequest));
        Integer stepTwoRequest = model.getStepTwoRequest();
        ui.getStepTwoRequestLabel().setText(t("cali.calibration.ask.value", stepTwoRequest));

        ui.getStepOne().setBackground(getInputBg(false));
        ui.getStepOneRequestLabel().setFont(getInputFont(false));
        ui.getStepTwo().setBackground(getInputBg(false));
        ui.getStepTwoRequestLabel().setFont(getInputFont(false));

        ui.getModel().start();
    }

    private void onInitializeChanged(boolean newValue) {
        ui.getStartMeasureMode().setEnabled(false);
        onStepValidChanged(newValue, ui.getInitializeResult(), ui.getInitializeResultStatus());
        ui.getStepOne().setBackground(getInputBg(newValue));
        ui.getStepOneRequestLabel().setFont(getInputFont(newValue));
    }

    private void onStepOneValidChanged(boolean newValue) {
        onStepValidChanged(newValue, ui.getStepOneResult(), ui.getStepOneResultStatus());
        ui.getStepOne().setBackground(getInputBg(!newValue));
        ui.getStepOneRequestLabel().setFont(getInputFont(!newValue));

        ui.getStepTwo().setBackground(getInputBg(newValue));
        ui.getStepTwoRequestLabel().setFont(getInputFont(newValue));
    }

    private void onStepTwoValidChanged(boolean newValue) {
        onStepValidChanged(newValue, ui.getStepTwoResult(), ui.getStepTwoResultStatus());
        ui.getStepTwo().setBackground(getInputBg(!newValue));
        ui.getStepTwoRequestLabel().setFont(getInputFont(!newValue));
    }

    private Font getInputFont(boolean newValue) {
        return newValue ? ui.getFont().deriveFont(Font.BOLD) : ui.getFont();
    }

    private Color getInputBg(boolean newValue) {
        return newValue ? RunCali.INPUT_COLOR : RunCali.BG_COLOR;
    }

    private void onValidChanged(boolean newValue) {
        ui.getStartMeasureMode().setEnabled(newValue);
    }

    private void onStepValidChanged(boolean newValue, JLabelEditor resultLabel, JLabel resultIcon) {
        if (newValue) {
            resultLabel.setForeground(RunCali.OK_COLOR);
            resultIcon.setIcon(RunCali.ICON_ACCEPTED.get());
        } else {
            resultLabel.setForeground(RunCali.WAITING_COLOR);
            resultIcon.setIcon(RunCali.ICON_WAITING.get());
        }
    }

}
