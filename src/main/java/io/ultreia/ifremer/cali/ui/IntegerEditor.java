package io.ultreia.ifremer.cali.ui;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializer;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerSupport;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.number.NumberEditor;

import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.awt.Font;
import java.util.Objects;

/**
 * Created by tchemit on 11/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class IntegerEditor extends NumberEditor {

    public IntegerEditor(JAXXContext parentContext) {
        super(parentContext);
        setNumberType(Integer.class);
        setUseSign(false);
        setNumberPattern(SwingUtil.INT_3_DIGITS_PATTERN);
    }

    public void setEditable(boolean editable) {
        getTextField().setEditable(editable);
    }
    public void registerAction(ApplicationAction action) {
        String actionCommandKey = action.getActionCommandKey();
        getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), actionCommandKey);
        getActionMap().put(actionCommandKey, action);
    }

    @AutoService(ComponentInitializer.class)
    public static class NumberBeanEditorInitializer extends ComponentInitializerSupport<IntegerEditor> {

        public NumberBeanEditorInitializer() {
            super(IntegerEditor.class);
        }

        @Override
        public void init(JAXXObject ui, IntegerEditor component) {
            JavaBean bean = (JavaBean) Objects.requireNonNull(ui.getObjectById("model"));
            component.setBean(bean);
            component.setProperty(component.getName());
            component.init();
            component.getTextField().setFont(component.getTextField().getFont().deriveFont(Font.BOLD));
            bean.addPropertyChangeListener(component.getModel().getConfig().getProperty(), e -> onValueChanged((Integer) e.getNewValue(), component));
        }

        private void onValueChanged(Integer newValue, IntegerEditor ui) {
            if (!Objects.equals(newValue, ui.getModel().getNumberValue())) {
                ui.setNumberValue(newValue);
            }
        }
    }

}
