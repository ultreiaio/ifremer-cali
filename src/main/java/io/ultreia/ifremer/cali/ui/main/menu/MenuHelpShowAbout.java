package io.ultreia.ifremer.cali.ui.main.menu;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.main.MainUI;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class MenuHelpShowAbout extends MenuActionSupport {

    public MenuHelpShowAbout() {
        super(t("cali.action.about"), t("cali.action.about.tip"), "about", 'A');
    }

    @Override
    protected void doActionPerformed0(ActionEvent event, MainUI ui) {
//
//        String name = "observe";
//
//        String changelog = null;
//        try {
//            try (Reader resource = new InputStreamReader(getClass().getClassLoader().getResourceAsStream("META-INF/" + name + "-CHANGELOG.md"), StandardCharsets.UTF_8)) {
//                Node node = Parser.builder().build().parseReader(resource);
//                changelog = HtmlRenderer.builder().build().render(node);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        String aboutContent = UiMainTextTemplates.applyAboutTemplate(ui.getConfig(), ui.getConfig().getApplicationLocale());
//
//        AboutUI about = AboutUIBuilder.builder(ui)
//                .setIconPath("/icons/logo.png")
//                .setTitle(t("observe.title.about"))
//                .setBottomText(ui.getConfig().getCopyrightText())
//                .addAboutTab(aboutContent, true)
//                .addDefaultLicenseTab(name, false)
//                .addDefaultThirdPartyTab(name, false)
//                .addTab(t("aboutframe.changelog"), changelog, true)
//                .build();
//
//        // tweak top panel
//        JPanel topPanel = about.getTopPanel();
//        topPanel.removeAll();
//        topPanel.setLayout(new BorderLayout());
//        topPanel.add(new JLabel(SwingUtil.getUIManagerIcon("project")), BorderLayout.WEST);
//        topPanel.add(new JLabel(SwingUtil.getUIManagerIcon("organization")), BorderLayout.EAST);
//
//        about.display();

    }
}
