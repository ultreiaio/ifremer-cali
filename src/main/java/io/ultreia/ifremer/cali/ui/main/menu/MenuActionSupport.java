package io.ultreia.ifremer.cali.ui.main.menu;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.main.MainUI;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;

import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.event.ActionEvent;

/**
 * Created by tchemit on 17/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class MenuActionSupport extends ApplicationAction<MainUI> {

    private boolean canEdit = false;

    public MenuActionSupport(String label, String shortDescription, String actionIcon, char mnemonic) {
        super(label, shortDescription, actionIcon, mnemonic);
        setKeyStroke(KeyStroke.getKeyStroke((int) mnemonic, 0));
        setDisplayMnemonicIndexKey(0);
        setAddKeyStrokeToText(false);
        setAddMnemonicAsKeyStroke(false);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return canEdit && super.canExecuteAction(e);
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, MainUI mainUI) {
        doActionPerformed0(e, mainUI);
        canEdit = false;
    }

    protected abstract void doActionPerformed0(ActionEvent e, MainUI mainUI);

    @Override
    public void init() {
        defaultInit(null, null);
        if (editor == null) {
            return;
        }

        JPopupMenu popupMenu = (JPopupMenu) editor.getParent();
        popupMenu.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                canEdit = true;
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                canEdit = false;
            }
        });

    }

}
