package io.ultreia.ifremer.cali.ui.main;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.RunCali;
import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import io.ultreia.java4all.lang.Strings;

import javax.swing.KeyStroke;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 09/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GetBoards extends CaliActionSupport<MainUI> implements Runnable {

    public GetBoards() {
        super(t("cali.main.getBoards"), t("cali.main.getBoards.tip"), "get-boards", KeyStroke.getKeyStroke("pressed F1"));
        setAddKeyStrokeToText(true);
    }

    @Override
    protected String getActionTitle() {
        return t("cali.main.getBoards.tip");
    }

    @Override
    public void run() {
        long t0 = System.nanoTime();
        try {
            ui.getBoardsLabel().setForeground(RunCali.WAITING_COLOR);
            ui.getBoardsLabel().setText(t("cali.main.boards.detecting"));

            ui.displayInfo("Démarrage de la détection des règles disponibles...");
            List<String> boards = ui.getContext().getBoardApi().getBoardNames();
            ui.getModel().setDetectedBoards(boards);
            int size = ui.getModel().getDetectedBoards().size();
            String time = Strings.convertTime(t0, System.nanoTime());
            ui.displayInfo(String.format("Détéction des règles disponibles terminée : %d règle(s) trouvée(s) temps de recherche %s).", size, time));
        } catch (Exception e) {
            ui.getContext().handlingError("Immposible de récupérer la liste des règles", e);
            ui.getModel().setDetectedBoards(Collections.emptyList());
        } finally {
            ui.getContext().unlock();
        }
    }

}
