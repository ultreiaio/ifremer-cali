package io.ultreia.ifremer.cali.ui.main.menu;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliConfig;
import io.ultreia.ifremer.cali.ui.main.MainUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.net.URL;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class MenuHelpGotoSite extends MenuActionSupport {

    private static final Log log = LogFactory.getLog(MenuHelpGotoSite.class);

    public MenuHelpGotoSite() {
        super(t("cali.action.site"), t("cali.action.site.tip"), "site", 'S');
    }

    @Override
    protected void doActionPerformed0(ActionEvent event, MainUI ui) {

        CaliConfig config = getUi().getConfig();

        URL siteURL = config.getApplicationSiteUrl();

        log.info(String.format("goto application site: %s", siteURL));
        if (Desktop.isDesktopSupported() &&
                Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(siteURL.toURI());
            } catch (Exception ex) {
                ui.getContext().handlingError(ex);
            }
        }

    }

}
