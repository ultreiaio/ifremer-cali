/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#body {
  opaque:true;
}

#mainFrame {
  defaultCloseOperation: "do_nothing_on_close";
  iconImage: {SwingUtil.createImageIcon("cali.png").getImage()};
}

#menuFile {
  enabled:{!config.isBusy()};
  text: "cali.menu.file";
  mnemonic: F;
}

#menuFileToFullScreen {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuFileToFullScreen.class";
  visible:{!config.isFullScreen()}
}

#menuFileToWindowScreen {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuFileToWindowScreen.class";
  visible:{config.isFullScreen()}
}

#menuFileReloadApplication {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuFileReloadApplication.class";
}

#menuFileCloseApplication {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuFileCloseApplication.class";
}

#menuConfiguration {
  text: "cali.menu.configuration";
  toolTipText: "cali.menu.configuration.tip";
  mnemonic: C;
  enabled:{!config.isBusy()};
}

#menuConfigurationToFrench {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuConfigurationToFrench.class";
}

#menuConfigurationToEnglish {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuConfigurationToEnglish.class";
}

#menuHelp {
  text: "cali.menu.help";
  mnemonic: A;
  enabled:true;
}

#menuHelpSite {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuHelpGotoSite.class";
}

#menuHelpAbout {
  actionType:"io.ultreia.ifremer.cali.ui.main.menu.MenuHelpShowAbout.class";
  enabled:false;
}

#getBoards {
  horizontalAlignment:{JLabel.CENTER};
  actionType:"io.ultreia.ifremer.cali.ui.main.GetBoards.class";
}

#selectBoard {
  horizontalAlignment:{JLabel.CENTER};
  actionType:"io.ultreia.ifremer.cali.ui.main.SelectBoard.class";
}

#boardsLabel {
  horizontalAlignment:{JLabel.CENTER};
  font-weight:bold;
}

#boardsPanel {
  font-size:14;
}

#boardsDetectionLabel {
  font-size:12;
  horizontalAlignment:{JLabel.CENTER};
}

#boardSelectedLabel {
  horizontalAlignment : {JLabel.CENTER};
  verticalAlignment : {JLabel.CENTER};
  visible:false;
}

#selectBoardPanel {
  visible:false;
}

#selectedBoardPanel {
  visible:false;
}

#status {
  showBusy:true;
  busy: {config.isBusy()};
}
