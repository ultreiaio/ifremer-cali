# Ifremer Cali by Ultreia.io

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.ifremer/cali.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.ifremer%22%20AND%20a%3A%22cali%22)
[![Build Status](https://gitlab.com/ultreiaio/ifremer-cali/badges/develop/build.svg)](https://gitlab.com/ultreiaio/ifremer-cali/pipelines)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/ifremer-cali/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/ifremer-cali)

# Community

* [Contact](mailto:dev@tchemit.fr)
